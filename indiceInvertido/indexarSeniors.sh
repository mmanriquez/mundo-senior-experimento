#!/bin/sh

ENDPOINT="http://localhost:9200"

curl -X POST "$ENDPOINT/trabajosenior/senior/_bulk" \
  -d '{"index": {}}
{"tipoDocumento":"senior", "id":"2","sexo":"t3e_sex_men","profesion":"qa company","titulo":"Diseñador","lugarEstudios":"Duoc","situacionLaboral":"t3e_labor_situation_employee","descripcion":"Usuario de qa","razonLaboral":"Porque solo tengo 22","ingresos":"123","comoNosEncontraste":"Haciendo la pagina","manejoComputacional":"t3e_leve_full","comuna":"Ollagüe"}
{"index": {}}
{"tipoDocumento":"senior", "id":"5","sexo":"t3e_sex_women","profesion":"qa company","titulo":"Programdor","lugarEstudios":"U de chile","situacionLaboral":"t3e_labor_situation_employee","descripcion":"Usuario de qa","razonLaboral":"","ingresos":"","comoNosEncontraste":"","manejoComputacional":"t3e_leve_full","comuna":"Las Condes"}'