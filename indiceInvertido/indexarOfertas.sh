#!/bin/sh

ENDPOINT="http://localhost:9200"

curl -X POST "$ENDPOINT/trabajosenior/oferta/_bulk" \
  -d '{"index": {}}
{"idCrawler":"10","nombre":"Secretaria","descripcion":"Buscamos a secretaria con manejo adecuado de Excell y Word.","fechaPublicacion":"2014-01-04 21:52:15","perfilPostulante":"","tipoHorario":"t3e_full_day","horarioSemana":"9:00-18:00","horarioSabado":"No","horarioDomingo":"No","localidad":"Las Condes","sueldo":"300.000","estado":"t3e_state_suspended"}
{"index": {}}
{"idCrawler":"11","nombre":"Supervisor control de calidad y despacho","descripcion":"Encargado de control de calidad de impresión publicitaria y despacho a clientes mediante flota de camionetas  de la empresa","fechaPublicacion":"2014-01-06 14:25:18","perfilPostulante":"","tipoHorario":"t3e_full_day","horarioSemana":"9 a 19 horas","horarioSabado":"9 a 17","horarioDomingo":"0","localidad":"El Bosque","sueldo":"500000","estado":"t3e_state_suspended"}'