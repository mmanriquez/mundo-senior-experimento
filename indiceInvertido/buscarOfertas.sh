#!/bin/sh

# Ejecutar como: ./buscarOfertas.sh "mis keywords"

ENDPOINT="http://localhost:9200"
QUERY=${1:-ventas}

echo "Buscando $QUERY en Ofertas Laborales..."

curl -X POST "$ENDPOINT/trabajosenior/oferta/_search?pretty=" \
  -H "content-type: application/json" \
  -d "{
  \"from\": 0,
  \"size\": 10,
    \"query\": {
      \"multi_match\": {
        \"query\": \"$QUERY\",
        \"type\": \"best_fields\",
        \"fields\": [ \"nombre\",  \"descripcion\", \"perfilPostulante\" ]
      }
    }
  }"