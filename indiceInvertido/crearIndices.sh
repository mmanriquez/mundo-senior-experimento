#!/bin/sh

ENDPOINT="http://localhost:9200"

curl -X PUT "$ENDPOINT/trabajosenior?pretty=" -H 'content-type: application/json' \
  -d '{
  "mappings": {
    "oferta": {
      "properties": {
        "idCrawler": { "type": "keyword" },
        "tipoDocumento": { "type": "keyword" },
        "nombre": { "type": "text", "analyzer": "spanish", "search_analyzer": "spanish" },
        "descripcion": { "type": "text", "analyzer": "spanish", "search_analyzer": "spanish" },
        "perfilPostulante": { "type": "text", "analyzer": "spanish", "search_analyzer": "spanish" },
        "tipoHorario": { "type": "keyword" },
        "horarioSemana": { "type": "text", "analyzer": "spanish", "search_analyzer": "spanish" },
        "horarioSabado": { "type": "text", "analyzer": "spanish", "search_analyzer": "spanish" },
        "horarioDomingo": { "type": "text", "analyzer": "spanish", "search_analyzer": "spanish" },
        "sueldo": { "type": "keyword" },
        "localidad": { "type": "keyword" },
        "keywordsProfesiones": { "type": "keyword" },
        "keywordsComunas": { "type": "keyword" },
        "fechaPublicacion": { "type": "date", "format": "yyyy-MM-dd HH:mm:ss||dd-MM-yyyy||strict_date_optional_time||epoch_millis", "ignore_malformed": true }
      }
    },
    "senior": {
      "properties": {
      "tipo": { "type": "keyword" },
      "id": { "type": "keyword" },
      "sexo": { "type": "keyword" },
      "profesion": { "type": "text", "analyzer": "spanish", "search_analyzer": "spanish" },
      "titulo": { "type": "text", "analyzer": "spanish", "search_analyzer": "spanish" },
      "lugarEstudios": { "type": "text", "analyzer": "spanish", "search_analyzer": "spanish" },
      "situacionLaboral": { "type": "keyword" },
      "descripcion": { "type": "text", "analyzer": "spanish", "search_analyzer": "spanish" },
      "razonLaboral": { "type": "text", "analyzer": "spanish", "search_analyzer": "spanish" },
      "ingresos": { "type": "keyword" },
      "comoNosEncontraste": { "type": "text", "analyzer": "spanish", "search_analyzer": "spanish" },
      "manejoComputacional": { "type": "keyword" },
      "comuna": { "type": "keyword" },
      "entidad": { "type": "keyword" },
      "numeroContacto": { "type": "keyword" },
      "correoContacto": { "type": "keyword" },
      "categoria": { "type": "keyword" },
      "tipoClasificado": { "type": "keyword" },
      "keywordsProfesiones": { "type": "keyword" },
      "keywordsComunas": { "type": "keyword" },
      "fechaPublicacion": { "type": "date", "format": "yyyy-MM-dd HH:mm:ss||dd-MM-yyyy||strict_date_optional_time||epoch_millis", "ignore_malformed": true }
      }
    }
  }
}'