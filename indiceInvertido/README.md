# MundoSenior - Poblar Índice Invertido

Para el módulo de índice invertido del sistema de recomendación, se debe instalar Java 8 y ElasticSearch 5.4+.

## Instalar ElasticSearch

Todas las instrucciones de esta guía son para Ubuntu Server:

OpenJDK8
```
sudo apt-get update
sudo apt-get instal openjdk-8-jdk openjdk-8-jre
```

ElasticSearch 5.4
```
cd /tmp
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.4.0.deb
sudo dpkg -i elasticsearch-5.4.0.deb
sudo systemctl enable elasticsearch.service
sudo systemctl start elasticsearch.service
```

En máquinas de 2GB de RAM o menos es necesario disminuir la heap de ElasticSearch para evitar conflictos con otros procesos del sistema. En Ubuntu Server, se debe editar el archivo `/etc/elasticsearch/jvm.options` y editar los parámetros `-Xms` y `-Xmx`, dejándolos como `-Xms1g -Xmx1g`.

## Verificar instalación

ElasticSearch ocupa un API REST para cargar información, modificar configuraciones y hacer consultas. La URL por defecto en instalaciones locales es `http://localhost:9200`.

Para verificar la integridad de los índices de ElasticSearch, ejecutar el siguiente comando en la Terminal:

```
curl -X GET 'http://localhost:9200/_cat/health?v=&pretty='
```

Todas estas consultas se realizan con el paquete cURL en Bash.

## Siguientes pasos

El URL del servidor remoto actual con ElasticSearch es `http://162.243.48.125/recsys2`. Es posible usar los scripts bash disponibles en este directorio para realizar las tareas correspondientes, con los datos de Mundo Senior. Es importante cambiar la variable `ENDPOINT` al inicio de estos scripts por la URL de ElasticSearch.

## Ejemplos funcionales mínimos

### Crear indices

Para crear indices se usa la siguiente consulta:

```
curl -X PUT 'http://localhost:9200/indice1?pretty=' -H 'content-type: application/json' \
 -d '{
  "mappings": {
    "dato1": {
      "properties": {
        "id": { "type": "keyword" },
        "descripcion": { "type": "text", "analyzer": "spanish", "search_analyzer": "spanish" },
        "fecha": { "type": "date", "format": "strict_date_optional_time||epoch_millis", "ignore_malformed": true }
      }
    },
    "dato2": {
      "properties": {
        "id": { "type": "keyword" },
        "descripcion1": { "type": "text", "analyzer": "spanish", "search_analyzer": "spanish" },
        "descripcion2": { "type": "text", "analyzer": "spanish", "search_analyzer": "spanish" },
        "numero": { "type": "integer" },
        "relevante":  { "type": "boolean" }
      }
    }
  }
}'
```

Esto crea el índice `indice1` con los tipos de dato `dato1` y `dato2`.

### Indexar datos

Para indexar datos en masa, se requiere un archivo JSON con el siguiente formato:

```
{ "index": {} }
{ "id": "a001", "descripcion": "Mi texto buscado como full-text search", "fecha": "2017-06-31" }
{ "index": {} }
{ "id": "a002", "descripcion": "Otro texto buscado como full-text search", "fecha": "2017-07-01" }
```

La siguiente consulta indexa los datos de tipo `dato1` sobre el `indice1`:

```
curl -X POST 'http://localhost:9200/indice1/dato1/_bulk' \
  -d '{ "index": {} }
{ "id": "a001", "descripcion": "Mi texto buscado como full-text search", "fecha": "2017-06-31" }
{ "index": {} }
{ "id": "a002", "descripcion": "Otro texto buscado como full-text search", "fecha": "2017-07-01" }'
```

### Realizar consultas sobre el índice:

Para búsquedas full-text search se utiliza la siguiente consulta (buscar por "best match" `ositos cariñositos` sobre datos de tipo `dato2` en `indice1`):

```
curl -X POST 'http://localhost:9200/indice1/dato2/_search?pretty=' \
  -H 'content-type: application/json' \
  -d '{
  "from": 0,
  "size": 10,
    "query": {
      "multi_match": {
        "query": "ositos cariñositos",
        "type": "best_fields",
        "fields": [ "descripcion1",  "descripcion2" ]
      }
    }
  }'
```