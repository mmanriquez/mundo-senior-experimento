#!/bin/sh

# Ejecutar como: ./buscarSeniors.sh "mis keywords"

ENDPOINT="http://localhost:9200"
QUERY=${1:-ventas}

echo "Buscando $QUERY en Seniors..."

curl -X POST "$ENDPOINT/trabajosenior/senior/_search?pretty=" \
  -H "content-type: application/json" \
  -d "{
  \"from\": 0,
  \"size\": 10,
    \"query\": {
      \"multi_match\": {
        \"query\": \"$QUERY\",
        \"type\": \"best_fields\",
        \"fields\": [ \"profesion\", \"titulo\", \"descripcion\" ]
      }
    }
  }"